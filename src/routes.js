const routes = [
  {
    icon: "mdi-apps",
    title: "Welcome",
    to: "/",
  },
  {
    icon: "mdi-file",
    title: "File Upload",
    to: "/file-upload",
  },
  {
    icon: "mdi-account",
    title: "Name Select",
    to: "/multi-select",
  },
  {
    icon: "mdi-tag",
    title: "Tags",
    to: "/tag-creator",
  },
  {
    icon: "mdi-pen",
    title: "Slot",
    to: "/slot-explore",
  },
];

export default routes;
