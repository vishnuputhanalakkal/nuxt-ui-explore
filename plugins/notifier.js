export default ({app, store}, inject) => {
    inject("notifier", {
        showMessage({message="", color="", timeout=5000}) {
            store.commit("snackbar/showMessage", {message, color, timeout})
        }
    })
}