export const state = () => ({
  message: "",
  color: "",
  timeout: 5000,
});

export const mutations = {
  showMessage(state, payload) {
    state.message = payload.message;
    state.color = payload.color;
    state.timeout = payload.timeout;
  },
};
